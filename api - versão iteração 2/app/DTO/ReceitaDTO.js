function ReceitaDTO(receita) {
    var prescricoesIDs = [];

    for (var i = 0; i < receita.prescricoes.length; i++) {
        prescricoesIDs.push(receita.prescricoes[i].id);
    }

    return {
        id: receita._id,
        utente: receita.utente,
        medico: receita.medico,
        data: receita.data,
        prescricoes: prescricoesIDs
    };
}

module.exports = ReceitaDTO;