function PrescricaoDTO(prescricao) {

    var datasAviamentos = [];
    for (var i = 0; i < prescricao.listaAviamentos.length; i++) {
        datasAviamentos.push(prescricao.listaAviamentos[i].dataAviamento);
    }

    return {
        id: prescricao._id,
        apresentacaoID: prescricao.apresentacaoID,
        validade: prescricao.validade,
        posologiaPrescrita: {
            quantidade: prescricao.posologiaPrescrita.quantidade,
            dias : prescricao.posologiaPrescrita.dias,
            intervalo_horas: prescricao.posologiaPrescrita.intervalo_horas
        },
        aviamentos: datasAviamentos 
    };
}

module.exports = PrescricaoDTO;