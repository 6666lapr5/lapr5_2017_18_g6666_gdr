function AviamentoDTO(aviamento, idPrescricao) {

    return {
        dataAviamento : aviamento.dataAviamento,
        farmaceutico : aviamento.farmaceutico,
        nAviamentos : aviamento.nAviamentos,
        apresentacao : aviamento.apresentacao,
        idPrescricao : idPrescricao
    };
}

module.exports = AviamentoDTO;