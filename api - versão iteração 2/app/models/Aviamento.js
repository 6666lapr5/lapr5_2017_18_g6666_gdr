var mongoose     = require('mongoose');

function Aviamento() {
    return  {
        dataAviamento : Date,
        farmaceutico : {type: mongoose.Schema.Types.ObjectId, ref:'User'},
        nAviamentos : Number,
        apresentacao : String
    };
}

module.exports = Aviamento;