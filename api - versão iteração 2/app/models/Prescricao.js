var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var Schema = mongoose.Schema;

var PrescricaoSchema = new Schema({
    apresentacao: String,
    apresentacaoID: Number,
    validade: Date,
    aviada: Boolean,
    posologiaPrescrita: {
        quantidade: Number,
        dias: Number,
        intervalo_horas: Number
    },
    listaAviamentos: [{
        dataAviamento: Date,
        farmaceutico: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        nAviamentos: Number
    }]
});

PrescricaoSchema.plugin(mongoose_validator);
module.exports = mongoose.model('Prescricao', PrescricaoSchema);