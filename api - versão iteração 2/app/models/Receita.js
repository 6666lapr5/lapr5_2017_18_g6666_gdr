var mongoose     = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var Schema       = mongoose.Schema;

var ReceitaSchema = new Schema({
    medico : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    utente : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    data: Date,
    prescricoes :[{
        apresentacao : String,
        apresentacaoID : String,
        aviada : Boolean,
        validade : Date,
        posologiaPrescrita : {
            quantidade : Number,
            dias : Number,
            intervalo_horas : Number
        },
        listaAviamentos:[{
            dataAviamento : Date,
            farmaceutico : {type: mongoose.Schema.Types.ObjectId, ref:'User'},
            nAviamentos : Number
        }]
    }]
});

ReceitaSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Receita', ReceitaSchema);