var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var Schema = mongoose.Schema;

var ComentarioSchema = new Schema({
    farmaco: String,
    forma: String,
    concentracao: String,
    quantidade: String,
    comentarios: []
});

ComentarioSchema.plugin(mongoose_validator);
module.exports = mongoose.model('Comentario', ComentarioSchema);