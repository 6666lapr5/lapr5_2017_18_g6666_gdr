var cryptoJS = require('node-cryptojs-aes').CryptoJS;

var key = ')DIE/z@q0>#n8ux?9k.eFZ%?Y.R~}_';

function encrypt (unencrypted){
    var encrypted = cryptoJS.AES.encrypt(unencrypted, key);
    return encrypted;
}

function decrypt (encrypted){
    //make this not work with empty string
    if(encrypted){
        var unencrypted = cryptoJS.AES.decrypt(encrypted, key);
        return unencrypted.toString(cryptoJS.enc.Utf8);
    }
}

module.exports = {
    encrypt : encrypt,
    decrypt : decrypt
};

