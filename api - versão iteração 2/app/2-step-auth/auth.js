'use strict';

var authenticator = require('authenticator');


function googleAuthenticateCreate(accountEmail) {
    var formattedKey = authenticator.generateKey();
    
    var otp = authenticator.generateTotpUri(formattedKey, accountEmail, "ACME Co", 'SHA1', 6, 30);
    //console.log(otp);

    return formattedKey;
}

function googleAuthenticateVerify(formattedKey, token) {

    return authenticator.verifyToken(formattedKey, token);

}

module.exports = {
    googleAuthenticateCreate: googleAuthenticateCreate,
    googleAuthenticateVerify: googleAuthenticateVerify
};

