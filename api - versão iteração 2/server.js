// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var Client = require('node-rest-client').Client;
var client = new Client();
var morgan = require('morgan');
var Comentario = require('./app/models/Comentario');

var port = process.env.PORT || 8080;        // set our port

const http = require('http').Server(app);

const socketIo = require('socket.io');
let io;

io = socketIo(http);

const encryption = require('./app/Encryption/encryption');
var googleAuth = require('./app/2-step-auth/auth');
var mongoose = require('mongoose');
var Receita = require('./app/models/Receita');
var Prescricao = require('./app/models/Prescricao');
var Aviamento = require('./app/models/Aviamento');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file
var bcrypt = require('bcryptjs');
var User = require('./app/models/User');
var eventos = require('events');
var eventEmitter = new eventos.EventEmitter();
var ReceitaDTO = require('./app/DTO/ReceitaDTO');
var PrescricaoDTO = require('./app/DTO/PrescricaoDTO');
var AviamentoDTO = require('./app/DTO/AviamentoDTO');
var PrescricaoDTOfarm = require('./app/DTO/PrescricaoDTOfarm');
var nodemailer = require("nodemailer");
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'Gmail',
    auth: {
        user: "testearqsidiogo@gmail.com",
        pass: "testearqsi2017"
    }
}));
var login = require('facebook-chat-api');
var cors = require('cors');
app.use(cors());
//TESTES DE FACEBOOK MESSAGE
/*login({ email: "testearqsidiogo@gmail.com", password: "testearqsi2017" }, (err, api) => {
    if (err) return console.error(err);

    var yourID = "100000300368830";
    var idGus = "100001130041930";
    var idPeaches = "100000719850588";
    var idVasco = "100009283359934";
    var msg = "Welcome";

    api.listen((err, message) => {
        console.log(message.body);
        api.sendMessage(message.body, message.threadID);
    });
});*/

//client.get('http://localhost:8080/setup');

//mongodb://mcn:mcn@ds121575.mlab.com:21575/arqsi2 // connection string para MongoDB local

mongoose.connect(config.database, { useMongoClient: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!");
});
mongoose.Promise = global.Promise;

app.set('superSecret', config.secret);

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(morgan('dev'));

// ROUTES FOR OUR API
// =============================================================================
var apiRoute = express.Router();              // get an instance of the express Router
var router = express.Router();

notificacoes.call();

apiRoute.route('/User')

    .post(function (req, res) {

        var email = req.body.email;
        //Expressao regular baseada no RFC 5322
        var regexpEmail = new RegExp('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');

        var password = req.body.password;
        //Password tem de ter pelo menos 9 carateres, os quais têm de pertencer a pelo menos 3 dos 4 grupos: maiusculas, minusculas, numeros, simbolos
        var regexpPassword = new RegExp('(?=.{9,})(?=.*?[^\w\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*');

        //Verifica se o email e password fazem match com a expressão regular
        //        if (email == undefined || password == undefined || !email.test(regexpEmail) || !password.test(regexpPassword)) {
        //            res.status(400).json({ success: false, message: 'Email e/ou password contêm carateres não válidos.' });
        //            res.end();
        //        }

        var user = new User();
        user.name = req.body.name;
        hashedPassword = bcrypt.hashSync(req.body.password, 8);
        user.password = hashedPassword;
        user.admin = req.body.admin;
        user.email = req.body.email;
        user.medico = req.body.medico;
        user.utente = req.body.utente;
        user.farmaceutico = req.body.farmaceutico;
        user.facebook = req.body.facebook;
        var googleKey = googleAuth.googleAuthenticateCreate(user.email);
        user.googleKey = encryption.encrypt(googleKey);

        User.findOne({
            email: req.body.email
        }, function (err, found) {
            if (!found) {
                user.save(function (err) {
                    if (err) {
                        return res.status(500).json({ success: false, message: 'Não foi possivel registar o utilizador.' });
                        res.end()
                    }
                    console.log(googleKey)
                    return res.json({ message: 'Utilizador registado!', googlekey: googleKey });
                    res.end();
                });
            } else if (found) {
                return res.status(400).json({ success: false, message: 'Endereço de email já em uso.' });
                res.end()
            }
        });
    })

    .get(function (req, res) {
        User.find({}, function (err, users) {
            res.json(users);
            res.end()
        });
    });

apiRoute.get('/Medicamento/nome=:med/apresentacoes', function (req, res) {

    var args = {
        data: { email: "a@a.pt", password: "Qwerty1!" },
        headers: { "Content-Type": "application/json" }
    };

    var headerVal = "Bearer ";

    // get a new token for the user
    client.post("https://gdmlapr6666.azurewebsites.net/api/account/token", args, function (data, response) {
        headerVal += JSON.stringify(data.token).substring(1, JSON.stringify(data.token).length - 1);

        args = {
            path: { "med": req.params.med },
            headers: { "Authorization": headerVal }
        };

        client.get("https://gdmlapr6666.azurewebsites.net/api/medicamentos/nome=${med}", args, function (data, response) {

            args = {
                path: { "med_id": data.id },
                headers: { "Authorization": headerVal }
            };

            client.get("https://gdmlapr6666.azurewebsites.net/api/medicamentos/${med_id}/apresentacoes", args, function (data, reponse) {
                return res.status(response.statusCode).json(data);
                res.end();
            });
        });
    });
});

apiRoute.post('/authenticate', function (req, res) {

    var email = encryption.decrypt(req.body.email);

    //Expressao regular baseada no RFC 5322
    var regexpEmail = new RegExp('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');

    var password = encryption.decrypt(req.body.password);
    //Password tem de ter pelo menos 9 carateres, os quais têm de pertencer a pelo menos 3 dos 4 grupos: maiusculas, minusculas, numeros, simbolos
    var regexpPassword = new RegExp('(?=.{9,})(?=.*?[^\w\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*');

    //Verifica se o email e password fazem match com a expressão regular
    //    if (email == undefined || password == undefined || !email.test(regexpEmail) || !password.test(regexpPassword)) {
    //        return res.status(400).json({ success: false, message: 'Email e/ou password contêm carateres não válidos.' });
    //        res.end();
    //    }

    var farmaceutico = req.body.farmaceutico;
    //Verifica se o utilizador que está a tentar fazer login é farmaceutico
    if (farmaceutico == true) {

        //Se encontrar algum utilizador com o email especificado mas que nao seja farmaceutico, devolve erro
        //Significa que alguem esta a tentar fazer login na farmacia e nao é farmaceutico
        User.findOne({ email: email, farmaceutico: false }, function (err, user) {
            if (err) {
                return res.status(500).json({ success: false, message: 'Autenticação falhada.' });
                res.end();
            }

            if (user) {
                return res.status(400).json({ success: false, message: 'Autenticação falhada.' });
                res.end();
            } else {
                login(farmaceutico);
            }
        });
    } else {
        login(farmaceutico);
    }

    function login(farmaceutico) {
        // find the user
        User.findOne({
            email: email
        }, function (err, user) {

            if (err) {
                res.status(500).json({ success: false, message: 'Autenticação falhada.' });
            } else if (!user) {
                res.status(404).json({ success: false, message: 'Autenticação falhada.' });
                res.end();
            } else if (user) {

                var validPassword = bcrypt.compareSync(password, user.password);

                // check if password matches
                if (!validPassword) {
                    res.status(401).send({ auth: false, token: null, message: 'Autenticação falhada.' });
                    res.end();
                } else {

                    // if user is found and password is right
                    // create a token with only our given payload
                    // we don't want to pass in the entire user since that has the password
                    var payload = {
                        id: user.id,
                        user: user.email,
                        medico: user.medico,
                        farmaceutico: user.farmaceutico,
                        utente: user.utente
                    };

                    if (farmaceutico) {
                        payload = {
                            id: user.id,
                            user: user.email,
                            medico: user.medico,
                            farmaceutico: user.farmaceutico,
                            utente: user.utente,
                            farmacia: user.farmacia
                        };
                    }

                    var token = jwt.sign(payload, app.get('superSecret'), {
                        expiresIn: 900 // expires in 15min
                    });

                    // return the information including token as JSON
                    
                    res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token
                    });
                    res.end();
                }
            }
        });
    };
});

apiRoute.post('/authGoogle', function (req, res) {
    var googleToken = encryption.decrypt(req.body.token);
    var email = encryption.decrypt(req.body.email);

    User.findOne({
        email: email
    }, function (err, user) {

        var formattedKey = encryption.decrypt(user.googleKey);

        var verif = googleAuth.googleAuthenticateVerify(formattedKey, googleToken);

        if (verif == null) {
            return res.status(401).json({ success: false, message: "Código Authenticator inválido" });
            res.end();
        } else {
            return res.json({ success: true, message: "Código válido" });
            res.end();
        }
    })
})

//Notificações de expiração
function notificacoes(req, res) {
    var utentes = [];
    var prescricoesExpirar = [];
    var query = Receita.find({});
    d = new Date();
    dif = 432000000; //diferença em milisegundos a testar para notificar

    query.select(`
            prescricoes._id 
            prescricoes.validade`);

    query.exec(function (err, result) {
        if (err) return res.end(err);
        var porAviar = [];
        result.forEach(function (receita) {

            Receita.findById(receita.id, function (err, receitaFound) {
                receita.prescricoes.forEach(function (prescricao) {
                    var difCalc = prescricao.validade.getTime() - d.getTime();
                    //console.log(difCalc);
                    if (difCalc < dif && difCalc > 0) {
                        //console.log(prescricao.id);
                        //console.log(receitaFound.utente + "ut");
                        utentes.push(receitaFound.utente);

                        var s = prescricao.id;
                        prescricoesExpirar.push(s);
                        eventEmitter.emit('enviarNotif', prescricao.id, receitaFound.utente);
                    }
                });
            })
        })

    });

    eventEmitter.on('enviarNotif', function (presc_id, user_id) {
        //EMAIL

        User.findById(user_id, function (err, user) {
            eventEmitter.emit('getUser', user);
        });

        eventEmitter.on('getUser', function (user) {
            transporter.sendMail({
                from: 'Notificacao Receitas <testearqsidiogo@gmail.com>',
                to: user.email,
                subject: 'Aviso de expiração!',
                html: "A sua prescrição " + presc_id + " expirará em breve",
            }, function (error, response) {
                //Email not sent
                if (error) {
                    return res.end("Email send Failed");
                }
                //email send sucessfully
                else {
                    console.log(response);
                }
                eventEmitter.emit('começarFacebook');
            })
            eventEmitter.on('começarFacebook', function () {
                //FACEBOOK
                login({ email: "testearqsidiogo@gmail.com", password: "testearqsi2017" }, (err, api) => {
                    if (err) return res.end("Facebook failed");

                    //console.log(user.facebook);

                    //api.sendMessage("A sua prescrição " + presc_id + " expirará em breve", "100000300368830");

                    api.getUserID(user.facebook, (err, data) => {
                        if (err) return res.end("getUser failed");
                        var msg = "A sua prescrição " + presc_id + " expirará em breve";
                        var threadID = data[0].userID;
                        api.sendMessage(msg, threadID);
                        //api.logout();
                    })

                    //api.logout();
                });
            })
        })
    })
    //console.log('Notificações enviadas');

};
/*
apiRoute.get('/Medicamento/:med_id/apresentacoes', function (req, res) {
    var args = {
        data: { email: "a@a.pt", password: "Qwerty1!" },
        headers: { "Content-Type": "application/json" }
    };
 
    headerVal = "Bearer ";
 
    // get a new token for the user
    client.post("https://gdmlapr6666.azurewebsites.net/api/account/token", args, function (data, response) {
        headerVal += JSON.stringify(data.token).substring(1, JSON.stringify(data.token).length - 1);
 
        args = {
            path: { "med_id": req.params.med_id },
            headers: { "Authorization": headerVal }
        };
 
        client.get("https://gdmlapr6666.azurewebsites.net/api/medicamento/${med_id}/apresentacoes", args, function (data, response) {
            return res.json(data);
            res.end();
        });
    });
});*/

apiRoute.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, app.get('superSecret'), function (err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
                res.end();
            } else {
                // if everything is good, save to request for use in other routes

                req.emailToken = decoded.user;
                //console.log(req.emailToken + ": " + decoded.email);

                User.findOne({
                    email: req.emailToken
                }, function (err, user) {
                    //console.log(user.email);
                    console.log(user.medico, user.farmaceutico, user.utente);
                    if (user.medico) {
                        req.userRole = "medico"
                    } else if (user.farmaceutico) {
                        req.userRole = "farmaceutico"
                    } else if (user.utente) {
                        req.userRole = "utente"
                    } else {
                        req.userRole = "admin"
                    }

                    eventEmitter.emit('continueRouting');
                    next();
                });
                //req.decoded = decoded;

            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'Não foi apresentado um token.'
        });
        res.end();

    }
});

// middleware to use for all requests
apiRoute.use(function (req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
apiRoute.get('/', function (req, res) {
    return res.json({ message: 'hooray! welcome to our api!' });
});

// more routes for our API will happen here
eventEmitter.once('continueRouting', function () {

    //api/Receita
    router.route('/Receita')

        .post(function (req, res) {
            console.log(req.userRole);
            if (req.userRole !== "medico" && req.userRole !== "admin") {
                return res.json("Não autorizado");
                res.end();
            }
            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                var args = {
                    data: { email: "a@a.pt", password: "Qwerty1!" },
                    headers: { "Content-Type": "application/json" }
                };

                var headerVal = "Bearer ";

                client.post("https://gdmlapr6666.azurewebsites.net/api/account/token", args, function (data, response) {
                    headerVal += JSON.stringify(data.token).substring(1, JSON.stringify(data.token).length - 1);

                    global.receita = new Receita();
                    receita.medico = user.id;
                    receita.utente = req.body.utente;
                    receita.data = new Date();
                    //global.prescricoes = req.body.prescricoes;

                    //var len = prescricoes.length;

                    /*for (var i = 0; i < len; i++) {
                        eventEmitter.on('getApresentacao', function (prescricoes, i) {
                            args = {
                                path: { "id": prescricoes[i].apresentacaoID },
                                headers: { "Authorization": headerVal }
                            };


                            client.get("https://gdmlapr6666.azurewebsites.net/api/apresentacao/${id}", args, function (data, response) {
                                var prescricao = new Prescricao();
                                prescricao.listaAviamentos = [];
                                prescricao.posologiaPrescrita = prescricoes[i].posologiaPrescrita;
                                prescricao.apresentacaoID = prescricoes[i].apresentacaoID;
                                prescricao.apresentacao = JSON.stringify(data);
                                prescricao.validade = new Date(prescricoes[i].validade);
                                prescricao.aviada = false;

                                receita.prescricoes[i] = prescricao;
                                if ((i + 1) == len) {
                                    saveReceita();
                                }
                            });
                        });

                        eventEmitter.emit('getApresentacao', prescricoes, i);
                    }*/
                    receita.save(function (err) {
                        if (err)
                            return res.end(err);

                        User.findById(receita.utente, function (err, utente) {
                            transporter.sendMail({
                                from: 'Notificacao Receita <testearqsidiogo@gmail.com>',
                                to: utente.email,
                                subject: 'Nova receita!',
                                html: "Foi-lhe passada uma receita com o id <b>" + receita._id + "</b>",
                            }, function (error, response) {
                                //Email not sent
                                if (error) {
                                    return res.end("Email send Failed");
                                }
                                //email send sucessfully
                                else {
                                    console.log(response);
                                }
                            });
                        });
                        return res.json({ message: 'Receita criada!' });
                        res.end();
                    });
                }
                );
            })
        })

        .get(function (req, res) {

            if (req.userRole !== "medico" && req.userRole !== "utente" && req.userRole !== "admin") {
                return res.json("Não autorizado");
                res.end();
            }

            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                //eventEmitter.on('getReceitas', function (user) {
                if (user.utente) {
                    Receita.find({ utente: user.id }, function (err, receitas) {
                        if (err) {
                            return res.send(err);
                            res.end();
                        }

                        var receitasDTO = [];
                        for (var i = 0; i < receitas.length; i++) {
                            receitasDTO.push(new ReceitaDTO(receitas[i]));
                        }
                        return res.json(receitasDTO);
                        res.end();
                    });
                } else if (user.medico) {
                    Receita.find({ medico: user.id }, function (err, receitas) {
                        if (err) {
                            return res.send(err);
                            res.end();
                        }

                        var receitasDTO = [];
                        for (var i = 0; i < receitas.length; i++) {
                            receitasDTO.push(new ReceitaDTO(receitas[i]));
                        }
                        return res.json(receitasDTO);
                        res.end();
                    });
                } else if (user.admin) {
                    Receita.find(function (err, receitas) {
                        if (err)
                            return res.end(err);

                        var receitasDTO = [];
                        for (var i = 0; i < receitas.length; i++) {
                            receitasDTO.push(new ReceitaDTO(receitas[i]));
                        }
                        return res.json(receitasDTO);
                        res.end();
                    });
                }
            })
        });


    router.route('/Receita/:receita_id')

        .get(function (req, res) {

            if (req.userRole !== "medico" && req.userRole !== "utente" && req.userRole !== "farmaceutico") {
                return res.status(403).json("Não autorizado");
                res.end();
            }

            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                Receita.findById(req.params.receita_id, function (err, receita) {
                    if (err) {
                        return res.status(404).send({ success: false, message: "Não foi encontrada a receita especificada." });
                        res.end();
                    }

                    if (receita.medico == user.id || receita.utente == user.id || req.userRole == "farmaceutico") {
                        var receitaDTO = new ReceitaDTO(receita);
                        return res.json(receitaDTO);
                        res.end();
                    }
                });
            })
        });

    router.route('/Receita/:receita_id/prescricao/:prescricao_id')

        .get(function (req, res) {

            /*if (req.userRole !== "medico" || req.userRole !== "utente") {
                return res.json("User role unauthorized");
            }*/
            //console.log(req.emailToken);
            User.findOne({
                email: req.emailToken

            }, function (err, user) {
                prescricaoPorId(user);
            });

            function prescricaoPorId(user) {
                Receita.findById(req.params.receita_id, function (err, receita) {

                    if (err) {
                        return res.status(500).send({ success: false, message: "Erro a procurar receita." });
                        res.end();
                    }

                    if (receita) {
                        for (var i = 0; i < receita.prescricoes.length; i++) {
                            if (receita.prescricoes[i]._id == req.params.prescricao_id) {
                                if (req.userRole == "utente" && user.id == receita.utente) {
                                    var prescricaoDTO = new PrescricaoDTO(receita.prescricoes[i]);
                                    return res.json(prescricaoDTO);
                                    res.end();

                                } else if (req.userRole == "medico" && user.id == receita.medico) {
                                    var prescricaoDTO = new PrescricaoDTO(receita.prescricoes[i]);
                                    return res.json(prescricaoDTO);
                                    res.end();

                                } else if (req.userRole == "farmaceutico") {
                                    var prescricaoDTO = new PrescricaoDTOfarm(receita.prescricoes[i]);
                                    return res.json(prescricaoDTO);
                                    res.end();
                                }
                            }
                        }
                    }

                    return res.status(404).json({ success: false, message: 'Prescrição não encontrada.' });
                    res.end();

                })
            }
        });


    router.route('/Receita/:receita_id/prescricao/:prescricao_id/aviar')
        .put(function (req, res) {

            if (req.userRole !== "farmaceutico" && req.userRole !== "admin") {
                return res.status(403).json("Não autorizado");
                res.end();
            }

            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                Receita.findById(req.params.receita_id, function (err, receita) {
                    if (err) {
                        return res.status(500).send({ success: false, message: "Erro a procurar receita." });
                        res.end();
                    }

                    if (receita) {
                        var d = new Date();
                        var prescricao;

                        for (var i = 0; i < receita.prescricoes.length; i++) {

                            if (receita.prescricoes[i]._id == req.params.prescricao_id) {

                                if (receita.prescricoes[i].validade >= d) {

                                    prescricao = receita.prescricoes[i];
                                    var aviamento = new Aviamento();
                                    aviamento.dataAviamento = new Date();
                                    aviamento.farmaceutico = user._id;
                                    aviamento.nAviamentos = req.body.nAviamentos;
                                    aviamento.apresentacao = prescricao.apresentacao;
                                    receita.prescricoes[i].listaAviamentos.push(aviamento);

                                    var AviamentoDTOe = new AviamentoDTO(aviamento, prescricao.id);

                                    receita.save(function (err) {
                                        if (err)
                                            return res.status(500).send({ success: false, message: "Não foi possível guardar o aviamento." })

                                        return res.json(AviamentoDTOe);
                                        res.end();
                                    });

                                } else {
                                    receita.prescricoes[i].aviada = true;

                                    receita.save(function (err) {
                                        if (err) {
                                            return res.status(500).send({ success: false, message: "Erro a colocar prescrição como aviada." })
                                        }
                                    });

                                    return res.status(404).send({ success: false, message: "Prescrição já foi aviada ou passou a validade." });
                                }
                            }
                        }
                    } else {
                        return res.status(404).send({ success: false, message: "Receita não encontrada." });
                    }
                });
            })
        });

    router.route('/Utente/:utente_id/prescricao/poraviar')
        .get(function (req, res) {

            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                if ((req.userRole !== "utente" && user._id !== req.params.utente_id) && req.userRole !== "admin") {
                    return res.status(403).json("Não autorizado");
                    res.end();
                } else {

                    Receita.find({ utente: user.id }, function (err, receitas) {
                        if (err)
                            return res.end(err);

                        var prescricoesDTO = [];
                        for (var i = 0; i < receitas.length; i++) {
                            var prescLen = receitas[i].prescricoes.length;
                            for (var j = 0; j < prescLen; j++) {
                                if (receitas[i].prescricoes[j].listaAviamentos.length == 0) {
                                    prescricoesDTO.push(new PrescricaoDTO(receitas[i].prescricoes[j]));
                                }
                            }
                        }
                        return res.json(prescricoesDTO);
                        res.end();
                    });
                }
            })
        });

    router.route('/Utente/:utente_id/prescricao/poraviar/:dia/:mes/:ano')
        .get(function (req, res) {

            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                if ((req.userRole !== "utente" && user._id !== req.params.utente_id) && req.userRole !== "admin") {
                    return res.status(403).json("Não autorizado");
                    res.end();
                } else {

                    Receita.find({ utente: user.id }, function (err, receitas) {
                        if (err)
                            return res.end(err);

                        var prescricoesDTO = [];
                        d = new Date();
                        d.setFullYear(req.params.ano, req.params.mes - 1, req.params.dia);
                        for (var i = 0; i < receitas.length; i++) {
                            var prescLen = receitas[i].prescricoes.length;
                            for (var j = 0; j < prescLen; j++) {
                                if (receitas[i].prescricoes[j].listaAviamentos.length == 0) {
                                    prescricoesDTO.push(new PrescricaoDTO(receitas[i].prescricoes[j]));
                                } else if (receitas[i].prescricoes[j].listaAviamentos[0].dataAviamento > d) {
                                    prescricoesDTO.push(new PrescricaoDTO(receitas[i].prescricoes[j]));
                                }
                            }
                        }
                        return res.json(prescricoesDTO);
                        res.end();
                    });
                }
            })
        });

    router.route('/Receita/:receita_id/prescricao')
        .post(function (req, res) {
            if (req.userRole !== "medico" && req.userRole !== "admin") {
                return res.status(403).json("Não autorizado");
                res.end();
            }

            User.findOne({
                email: req.emailToken
            }, function (err, user) {

                Receita.findById(req.params.receita_id, function (err, receita) {
                    if (err) {
                        return res.status(404).json({ success: false, message: 'Não foi encontrada a receita especificada.' });
                        res.end();
                    }
                    if (!(user.id == receita.medico || user.admin)) {
                        return res.status(403).json({ success: false, message: 'Não tem permissão para ver a receita.' });
                        res.end();
                    }


                    var args = {
                        data: { email: "a@a.pt", password: "Qwerty1!" },
                        headers: { "Content-Type": "application/json" }
                    };

                    headerVal = "Bearer ";

                    // get a new token for the user
                    client.post("https://gdmlapr6666.azurewebsites.net/api/account/token", args, function (data, response) {
                        headerVal += JSON.stringify(data.token).substring(1, JSON.stringify(data.token).length - 1);

                        args = {
                            path: { "id": req.body.apresentacaoID },
                            headers: { "Authorization": headerVal }
                        };

                        // get the apresentacao with that id
                        client.get("https://gdmlapr6666.azurewebsites.net/api/apresentacao/${id}", args, function (data, response) {
                            var prescricao = new Prescricao();
                            prescricao.apresentacao = JSON.stringify(data);
                            prescricao.apresentacaoID = req.body.apresentacaoID;
                            prescricao.validade = new Date(req.body.validade);
                            prescricao.aviada = false;
                            prescricao.listaAviamentos = [];
                            prescricao.posologiaPrescrita = req.body.posologiaPrescrita;
                            prescricao.nAviamentos = req.body.nAviamentos;

                            receita.prescricoes.push(prescricao);

                            // save the receita and check for errors
                            receita.save(function (err) {
                                if (err) {
                                    return res.status(500).son({ success: false, message: 'Não foi possível guardar a receita.' });
                                    res.end();
                                }

                                return res.json({ success: true, message: 'Prescrição adicionada.' });
                                res.end();
                            });
                        });
                    });
                });
            });
        });

    router.route('/farmaco/nome=:nome/apresentacao/:apresentacao_id/comentar')
        .post(function (req, res) {

            var token = req.body.token || req.query.token || req.headers['x-access-token'];

            jwt.verify(token, app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.status(400).json({ success: false, message: 'Falha na autenticação do token.' });
                    res.end();
                } else {
                    var emailToken = decoded.user;

                    User.findOne({
                        email: emailToken
                    }, function (err, user) {
                        if (user.medico || user.admin) {
                            eventEmitter.emit('comentar', user);
                        } else {
                            return res.status(403).json({ success: false, message: 'Não tem permissão para comentar apresentação de fármaco.' });
                            res.end();
                        }
                    });
                }
            });

            eventEmitter.on('comentar', function (user) {
                var args = {
                    data: { email: "a@a.pt", password: "Qwerty1!" },
                    headers: { "Content-Type": "application/json" }
                };

                headerVal = "Bearer ";

                // get a new token for the user
                client.post("https://gdmlapr6666.azurewebsites.net/api/account/token", args, function (data, response) {

                    headerVal += JSON.stringify(data.token).substring(1, JSON.stringify(data.token).length - 1);

                    args = {
                        path: { "nome": req.params.nome },
                        headers: { "Authorization": headerVal }
                    };

                    client.get("https://gdmlapr6666.azurewebsites.net/api/farmaco/nome=${nome}", args, function (data, response) {

                        args = {
                            path: { "id": data.id },
                            headers: { "Authorization": headerVal }
                        };

                        // get the apresentacao with that id
                        client.get("https://gdmlapr6666.azurewebsites.net/api/farmaco/${id}/apresentacoes", args, function (data, response) {

                            args = {
                                path: { "id": req.params.apresentacao_id },
                                headers: { "Authorization": headerVal }
                            };

                            client.get("https://gdmlapr6666.azurewebsites.net/api/apresentacao/${id}", args, function (data, response) {

                                Comentario.findOne({ farmaco: req.params.nome, forma: data.forma, concentracao: data.concentracao, quantidade: data.quantidade }, function (err, comentario) {
                                    if (comentario !== null) {
                                        comentario.comentarios.push(req.body.comentario);
                                    } else {
                                        comentario = new Comentario();
                                        comentario.farmaco = req.params.nome;
                                        comentario.forma = data.forma;
                                        comentario.concentracao = data.concentracao;
                                        comentario.quantidade = data.quantidade;
                                        comentario.comentarios.push(req.body.comentario);
                                    }

                                    // save the receita and check for errors
                                    comentario.save(function (err) {
                                        if (err) {
                                            return res.status(500).json({ success: false, message: 'Não foi possível guardar o comentário.' });
                                            res.end();
                                        }

                                        return res.json({ success: true, message: 'Comentário adicionado com sucesso.' });
                                        res.end();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
})
// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', apiRoute);

apiRoute.use("/", router);

// START THE SERVER
// =============================================================================
//app.listen(port);
let server = http.listen(port, () => {
    console.log('Express server listening on %d', port);
});
//console.log('Magic happens on port ' + port)
// make server exportable for testing
module.exports = app;
