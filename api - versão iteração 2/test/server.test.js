'use strict';

var app = require('../server'),
  assert = require('assert'),
  chai = require('chai'),
  request = require('supertest'),
  bodyParser = require('body-parser');
  const encryption = require('../app/Encryption/encryption');

var expect = chai.expect;


  describe('API Tests', function() { 
    var utToken,
        farmToken,
        medToken;


      it('GET /api/User', function(done) { 
        request(app) .get('/api/User')
          .end(function(err, res) { 
            expect(res.statusCode).to.equal(200); 
            expect(res.body[0].name).to.equal('Admin');
            done(); 
          }); 
      });

    it('should authenticate a user successfully', function(done) { 
      request(app).post('/api/authenticate').send({
        'email': 'U2FsdGVkX1/E2Zn3Qkv75maX/1S0CCgf+lZdoRLAzDArh0iXxbwQbE2RL2Ghoo7C',
        'password': 'U2FsdGVkX1+O6agtr/CJmyl9y0TzgBMtpmGLU1SVAQo='
      })
        .end(function(err, res) { 
        expect(res.statusCode).to.equal(200); 
        expect(res.body.message).to.equal('Enjoy your token!');
        medToken = res.body.token;
      }); 
      request(app).post('/api/authenticate').send({
        'email': 'U2FsdGVkX1+pqq2dsa+cgCEyFLo/qJYYwleNycsJPYMxPLUkQm0kazmYGetiKiaK',
        'password': 'U2FsdGVkX1+hjgBybmZhmnJqLapgSngmcrcGqMmjBq8='
      })
        .end(function(err, res) { 
        utToken = res.body.token;
      }); 
      request(app).post('/api/authenticate').send({
        'email': 'U2FsdGVkX19lt3/mQ9fUJLtj19/OrDKOgchjnkY/AhpC+vj+j0CpuWL4/4HfPk8U',
        'password': 'U2FsdGVkX181hPHsbxNvsm0CPcxTRHvsZAxQ6x/lC6Y='
      })
        .end(function(err, res) { 
        farmToken = res.body.token;
      }); 
      done(); 
    }); 

    it('GET /Medicamento/nome=Ben-u-ron/apresentacoes', function(done) { 
      request(app) .get('/api/Medicamento/nome=Ben-u-ron/apresentacoes')
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body.length).to.equal(3);
          done(); 
      });      
    });

  //AUTH GOOGLE

    it('POST /receita', function(done) { 
      request(app) .post('/api/receita').set('x-access-token', medToken).send({ utente: '5a53d41ef6cf861b0865fa4c' })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.message).to.equal('Receita criada!'); 
          done(); 
      });      
    });

    it('GET /receita', function(done) { 
      request(app) .get('/api/receita').set('x-access-token', medToken)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body[0].medico).to.equal('5a4e30134085313048643070');
          done(); 
      });      
    });

    it('GET /receita/:receita_id', function (done) {

      var id = '5a5790309bb60b3ef8390aad';

      request(app).get('/api/receita/5a5790309bb60b3ef8390aad').set('x-access-token', medToken).end(function (err, res) {
          
          expect(res.statusCode).to.equal(200); 
          expect(res.body.id).to.equal(id);
          done();

      });
  });
    
    it('GET /receita/:receita_id/prescricao/:prescricao_id', function (done) {

      var p_id = '5a5790309bb60b3ef8390aae';

      request(app).get('/api/receita/5a5790309bb60b3ef8390aad/prescricao/5a5790309bb60b3ef8390aae').set('x-access-token', medToken).end(function (err, res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body.id).to.equal(p_id);
          done();

      });
  });

    it('PUT /receita/:receita_id/prescricao/:prescricao_id/aviar', function (done) {

      var p_id = '5a5790309bb60b3ef8390aae';

      request(app).put('/api/receita/5a5790309bb60b3ef8390aad/prescricao/5a5790309bb60b3ef8390aae/aviar').send({ nAviamentos: 15 }).set('x-access-token', farmToken).end(function (err, res) {

          expect(res.body.idPrescricao).to.equal(p_id);
          expect(res.statusCode).to.equal(200);
          done();
      });
  });
  
    it('GET /Utente/:utente_id/prescricao/poraviar/:dia/:mes/:ano', function (done) {

      var u_id = '5a53dbb2062f3b08dc482abe';
      request(app).get('/api/utente/5a53dbb2062f3b08dc482abe/prescricao/poraviar/11/12/2018').set('x-access-token', utToken).end(function (err, res) {
          expect(res.statusCode).to.equal(200);
          done();

      });
  });

    it('POST /receita/:receita_id/prescricao', function (done) {

      var r_id = '5a5790309bb60b3ef8390aad';
      var p_id = '5a5790309bb60b3ef8390aae';
      	console.log(medToken);
      request(app).post('/api/receita/5a5790309bb60b3ef8390aad/prescricao').send({ apresentacaoID: '5',validade: '2018-12-11T00:00:00.000Z',posologiaPrescrita: {
        intervalo_horas: '10',
        dias: '5',
        quantidade: '2'
        },nAviamentos: '1000' }).set('x-access-token', medToken).end(function (err, res) {
          expect(res.body.message).to.equal('Prescrição adicionada.');
          expect(res.statusCode).to.equal(200);
          done();
      });
  });

  it('GET /Utente/:utente_id/prescricao/poraviar', function (done) {

    var email = encryption.encrypt('1150490@isep.ipp.pt').toString();
    var password = encryption.encrypt('password').toString();

    request(app).post('/api/authenticate').send({ email: email, password: password }).end(function (err, res) {

      var user_id = '5a53dbb2062f3b08dc482abe';
      var presc_poraviar = '5a5886c0b9fc5634a8df4a14';
      var token = res.body.token;

      request(app).get('/api/Utente/5a53dbb2062f3b08dc482abe/prescricao/poraviar').set('x-access-token', token).end(function (err, res) {
        expect(res.body[0].id).to.equal(presc_poraviar);
        done();
      });
    });
  });


});
